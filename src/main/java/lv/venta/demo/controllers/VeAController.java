package lv.venta.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import lv.venta.demo.services.IVeAService;

@Controller
public class VeAController {
	@Autowired
	IVeAService veaService;
	
	//get method for controller
	@GetMapping("/inputData")
	public String testingDataGet()//localhost:8080/inputData
	{
		veaService.createDataForTesting();
		return "ok";//ok.html
	}
}

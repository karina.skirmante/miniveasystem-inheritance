package lv.venta.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiniVeASystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiniVeASystemApplication.class, args);
	}

}

package lv.venta.demo.repos;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import lv.venta.demo.models.Course;
import lv.venta.demo.models.Grade;
import lv.venta.demo.models.Student;

public interface IGradeRepo extends CrudRepository<Grade, Long> {

	ArrayList<Grade> findByStudent(Student s1);//select * from Grade where student.id = s1.getId

	ArrayList<Grade>  findByStudentName(String studentName);

	ArrayList<Grade>  findByCourse(Course c1);

	ArrayList<Grade> findByGradeValueLessThan(int threshold);

	//SELECT AVG(GradeValue) FROM Grade WHERE C_ID=c1.getId()
	//FROM NameOfEntity
	@Query(value="SELECT avg (g.gradeValue) FROM Grade_Table g WHERE g.course = ?1")
	double calculateAvgGradeByCourse(Course c1);
	
	@Query(value = "SELECT avg(g.gradeValue) FROM Grade_Table g WHERE g.student = ?1")
	double calculateAvgGradeByStudent(Student student);
	

	
}

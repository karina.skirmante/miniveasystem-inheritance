package lv.venta.demo.models;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Table
@Entity(name = "Grade_Table")
@Getter @Setter @NoArgsConstructor
public class Grade {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "G_ID")
	@Setter(value=AccessLevel.PRIVATE)
	private long g_ID;
	

	@Column(name = "GradeValue")
	private int gradeValue;
	
	
	@ManyToOne
	@JoinColumn(name="S_ID")//link to column
	private Student student;
	
	
	
	@ManyToOne
	@JoinColumn(name="C_ID")
	private Course course;
	
	
	
	
	
	public Grade(int gradeValue) {
		this.gradeValue = gradeValue;
	}
	
	public Grade(int gradeValue, Student student, Course course) {
		this.gradeValue = gradeValue;
		this.student = student;
		this.course = course;
	}
	
	public String toString()
	{
		return ""+gradeValue;
	}
	
}

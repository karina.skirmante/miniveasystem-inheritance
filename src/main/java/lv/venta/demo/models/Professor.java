package lv.venta.demo.models;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Table
@Entity(name = "Professor_Table")
@Getter @Setter @NoArgsConstructor @ToString
public class Professor extends Person {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "P_ID")
    @Setter(value=AccessLevel.PRIVATE)
	private long p_ID;
/*
	@Column(name = "P_Name")
	private String name;

	@Column(name = "P_Surname")
	private String surname;
*/
	/*//relation _> only one professor is in Course
	@OneToOne(mappedBy="professor")//link to the variable
	private Course course;
	*/
	
	//many courses to one professor
	@ManyToMany(mappedBy="professors")
	private Collection<Course> courses;
	
		

	public Professor(String name, String surname) {
		super(name, surname);
	}

	

	

}

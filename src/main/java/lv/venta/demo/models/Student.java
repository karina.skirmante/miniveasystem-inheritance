package lv.venta.demo.models;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Table
@Entity(name = "Student_Table")
@Getter @Setter @NoArgsConstructor @ToString
public class Student extends Person {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "S_ID")
    @Setter(value=AccessLevel.PRIVATE)
    private long s_ID;

       
    @OneToMany(mappedBy = "student")//link to variable
    private Collection<Grade> grades;
    
   	public Student(String name, String surname) {
       super(name, surname);
    }

    


    
}
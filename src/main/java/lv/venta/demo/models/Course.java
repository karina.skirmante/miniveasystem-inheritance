package lv.venta.demo.models;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Table
@Entity(name = "Course_Table")
@Getter @Setter @NoArgsConstructor @ToString
public class Course {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "C_ID")
	@Setter(value=AccessLevel.PRIVATE)
	private long c_ID;
	@Column(name = "Title")
	private String title;
	@Column(name = "CreditPoints")
	private int creditPoints;
	
	/*//relation
	@OneToOne-> only one professor is in Coursey 
	//FK of Professor will be store in Course Table
	@JoinColumn(name="P_ID")//link to other table column
	private Professor professor;
	*/
	
	//many professors to one course
	@ManyToMany
	@JoinTable(name="Professor_Course", joinColumns=@JoinColumn(name="C_ID"), inverseJoinColumns=@JoinColumn(name="P_ID") )
	private Collection<Professor> professors;
	
	@OneToMany(mappedBy="course")
	private Collection<Grade> grades;
	
	
		
	
	public Course(String title, int creditPoints, Collection<Professor> professors) {
		this.title = title;
		this.creditPoints = creditPoints;
		this.professors = professors;
	}


	
	public Course(String title, int creditPoints) {
		super();
		this.title = title;
		this.creditPoints = creditPoints;
	}
	
	
	
}

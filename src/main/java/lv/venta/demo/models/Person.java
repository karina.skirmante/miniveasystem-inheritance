package lv.venta.demo.models;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@MappedSuperclass
@Setter @Getter @ToString @ NoArgsConstructor
public class Person {
	@Column(name = "P_Name")
	private String name;

	@Column(name = "P_Surname")
	private String surname;

	public Person(String name, String surname) {
		super();
		this.name = name;
		this.surname = surname;
	}
	
	
	
}

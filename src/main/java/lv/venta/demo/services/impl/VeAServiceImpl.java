package lv.venta.demo.services.impl;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lv.venta.demo.models.Course;
import lv.venta.demo.models.Grade;
import lv.venta.demo.models.Professor;
import lv.venta.demo.models.Student;
import lv.venta.demo.repos.ICourseRepo;
import lv.venta.demo.repos.IGradeRepo;
import lv.venta.demo.repos.IProfessorRepo;
import lv.venta.demo.repos.IStudentRepo;
import lv.venta.demo.services.IVeAService;

@Service
public class VeAServiceImpl implements IVeAService {

	@Autowired
	IStudentRepo studentRepo;
	
	@Autowired
	IProfessorRepo professorRepo;
	
	@Autowired
	ICourseRepo courseRepo;
	
	@Autowired
	IGradeRepo gradeRepo;
	
	@Override
	public void createDataForTesting() {
		//model testing where oneToOne between Course and Professor
		/*Student s1 = new Student("Janis", "Berzins");
		Student s2 = new Student("Baiba", "Jauka");
		Student s3 = new Student("Liga", "Kalnina");
		studentRepo.save(s1);
		studentRepo.save(s2);
		studentRepo.save(s3);
		
		Professor p1 = new Professor("Karina", "Skirmante");
		Professor p2 = new Professor("Estere", "Vitola");
		Professor p3 = new Professor("Raita", "Rollande");
		professorRepo.save(p1);
		professorRepo.save(p2);
		professorRepo.save(p3);
		
		Course c1 = new Course("JAVA", 4, p1);
		Course c2 = new Course("Python", 4, p2);
		Course c3 = new Course("ProgrInze", 6, p3);
		courseRepo.save(c1);
		courseRepo.save(c2);
		courseRepo.save(c3);
		
		Grade g1 = new Grade(10, s1, c1);
		gradeRepo.save(g1);
		gradeRepo.save(new Grade(8, s1, c2));
		
		gradeRepo.save(new Grade(3, s2, c1));
		gradeRepo.save(new Grade(6, s2, c2));
		gradeRepo.save(new Grade(4, s2, c3));
		
		gradeRepo.save(new Grade(10, s3, c1));
		gradeRepo.save(new Grade(9, s3, c2));
		gradeRepo.save(new Grade(10, s3, c3));
*/
		
		//model testing where manyToMany between Course and Professor
		Student s1 = new Student("Janis", "Berzins");
		Student s2 = new Student("Baiba", "Jauka");
		Student s3 = new Student("Liga", "Kalnina");
		studentRepo.save(s1);
		studentRepo.save(s2);
		studentRepo.save(s3);
		
		Professor p1 = new Professor("Karina", "Skirmante");
		Professor p2 = new Professor("Estere", "Vitola");
		Professor p3 = new Professor("Raita", "Rollande");
		professorRepo.save(p1);
		professorRepo.save(p2);
		professorRepo.save(p3);
		ArrayList<Professor> javaProfessors = new ArrayList<Professor>();
		javaProfessors.add(p1);
		
		Course c1 = new Course("JAVA", 4, javaProfessors);
		Course c2 = new Course("Python", 4, new ArrayList<>(Arrays.asList(p2)));
		Course c3 = new Course("ProgrInze", 6, new ArrayList<>(Arrays.asList(p1, p3)));
		courseRepo.save(c1);
		courseRepo.save(c2);
		courseRepo.save(c3);
		
		Grade g1 = new Grade(9, s1, c1);
		gradeRepo.save(g1);
		gradeRepo.save(new Grade(8, s1, c2));
		
		gradeRepo.save(new Grade(3, s2, c1));
		gradeRepo.save(new Grade(6, s2, c2));
		gradeRepo.save(new Grade(4, s2, c3));
		
		gradeRepo.save(new Grade(2, s3, c1));
		gradeRepo.save(new Grade(9, s3, c2));
		gradeRepo.save(new Grade(3, s3, c3));
		
		//Atrast kāda konkrēta studenta visas atzīmes
		System.out.println(gradeRepo.findByStudent(s1));
		//Atrast visas konkrēta studenta atzīmes pēc viņa vārda
		System.out.println("Janis atzimes:" +gradeRepo.findByStudentName("Janis"));
	
		//Atrast visas atzīmes konkrētā kursā
		System.out.println("JAVA atzimes" + gradeRepo.findByCourse(c1));
		
		//Atrast visas nesekmīgās atzīmes un šo atzīmju studentus
		ArrayList<Grade> allNegativesGrades = gradeRepo.findByGradeValueLessThan(4);
		System.out.println("nesekmígie visá VeA");
		for(int i = 0; i < allNegativesGrades.size();i++)
		{
			System.out.println(allNegativesGrades.get(i).getGradeValue() + " " + allNegativesGrades.get(i).getStudent().getName() + " " + allNegativesGrades.get(i).getCourse().getTitle());
		}
		
		
		//Atrast vidējo atzīmi konkrētā kursā
		System.out.println("JAVA vid.atzime" + gradeRepo.calculateAvgGradeByCourse(c1));
		
		//Atrast vidējo atzīmi konkrētam studentam
		System.out.println("Baiba vid.atzime" + gradeRepo.calculateAvgGradeByStudent(s2));
		
		
		
	}

}
